import React, { useEffect, useState } from 'react'

import CustomersDashboard from '../Components/Customers/CustomersDashboard';
import api from '../Api/api';
import ICustomer from '../Entities/customer';


const Customers = () => {

    const[editCustomer, setEditCustomer] = useState<boolean>(false)
    const[isListLoaded, setListLoaded] = useState<boolean>(false)
    const[customers, setCustomers] = useState<ICustomer[]>([])
    const[SelectedCustomer, setSelectedCustomer] = useState<ICustomer|null>(null)

    useEffect(() => {

        api.Customer.list().then((response) => {

            setCustomers(response.data)
            setListLoaded(true)

        })
        .catch((error) => {

            
            
        })
    }, [])


    const handleEditEvent = (customer:ICustomer|null) => {
        setEditCustomer(true);
        setSelectedCustomer(customer)
    }


    const handleCancelEvent = () => {
        setEditCustomer(false)
        setSelectedCustomer(null)
    }

    const handleSaveCustomer = (customer:ICustomer) => {
        if (customer?.id === 0) {
            api.Customer.create(customer).then((response) => {
                customers.push(response.data)
                setCustomers(customers)
                setEditCustomer(false)
                setSelectedCustomer(null)
            })
            .catch(() => {

            })
        }
        else{
            api.Customer.update(customer).then((Response) => {

                let index = customers.findIndex( u=> u.id === customer.id)
                customers[index] = customer

                setCustomers(customers)
                setEditCustomer(false)
                setSelectedCustomer(null)

            })
            .catch(() =>{

            })
        }
    }

    return(
        <div>
            <CustomersDashboard 
            selectedCustomer={SelectedCustomer}
            customers={customers} 
            isListLoaded={isListLoaded} 
            saveCustomerEvent={handleSaveCustomer}
            editCustomerEvent={handleEditEvent} 
            cancelEvent = {handleCancelEvent}
            editCustomer = {editCustomer}/>
        </div>
    )
}

export default Customers;