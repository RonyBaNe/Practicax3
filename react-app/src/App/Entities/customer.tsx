
export default interface ICustomer{
    id:number,
    name:string,
    rfc:string,
    phone:string,
    email:string,
    address:string
}