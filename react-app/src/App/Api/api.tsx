import axios, {AxiosResponse} from 'axios'

import ICustomer from '../Entities/customer'

axios.defaults.baseURL = 'https://localhost:7181/api'

const responseBody = (response: AxiosResponse) =>  response.data

const request = {
    get: (url: string) => axios.get(url).then(responseBody),
    
    post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
    
    put: (url: string, body: {}) => axios.put(url, body).then(responseBody),
}

const Customer = {
    list: () => request.get('customer'),
    
    create: (customer: ICustomer) => request.post('customer',customer),
    
    update: (customer: ICustomer) => request.put('customer',customer),
}

const api ={
    axios,
    Customer
}

export default api;