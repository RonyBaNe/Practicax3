import React, { useState } from 'react'

import ICustomer from '../../Entities/customer'

interface IProps{
    selectedCustomer: ICustomer|null,
    saveCustomerEvent:(customer:ICustomer) => void,
    cancelEvent : () => void,
}



const CustomerEdit = ({cancelEvent, selectedCustomer, saveCustomerEvent}:IProps) => {

    let deafultCustomer = {
        id: 0,
        name: '',
        rfc: '',
        phone: '',
        email: '',
        address: ''
    }

    let customerRef: ICustomer = (selectedCustomer != null)? selectedCustomer: deafultCustomer

    let label = customerRef.id === 0 ? 'New Customer': 'Edit Customer'

    const[customer, setcustomer] = useState<ICustomer>(customerRef)

    const handleInputChange = (event: any) => {
        const{name, value} = event.target
        setcustomer({...customer,[name]:value})
    } 

    return(
        <React.Fragment>
            <h3 className="mt-3 mb-3">{label}</h3>
            <form>
                <div className="mb-3">
                    <label  className="form-label">Name</label>
                    <input onChange={handleInputChange} type="text" className="form-control" id="name" value={customer.name}  name="name" aria-describedby="emailHelp"/>
                </div>
                <div className="mb-3">
                    <label className="form-label">RFC</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.rfc} id="rfc" name="rfc"/>
                </div>
                <div className="mb-3">
                    <label className="form-label">Phone</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.phone} id="phone" name="phone"/>
                </div>

                <div className="mb-3">
                    <label className="form-label">Address</label>
                    <input onChange={handleInputChange} type="text" className="form-control" value={customer.address} id="address" name="address"/>
                </div>
            
                <button type="button" onClick={() => saveCustomerEvent(customer)} className="btn btn-primary">Save</button>
                <button type='button' onClick={ () => cancelEvent()} className='btn btn-dark m-2'>Cancel</button>
            </form>
        </React.Fragment>
    )
}

export default CustomerEdit;