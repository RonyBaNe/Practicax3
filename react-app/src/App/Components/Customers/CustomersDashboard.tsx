import React from 'react'

import CustomerList from './CustomerList';
import CustomerEdit from './CustomerEdit';
import ICustomer from '../../Entities/customer';


interface IProps{
    customers: ICustomer[],
    editCustomer: boolean,
    isListLoaded: boolean,
    selectedCustomer: ICustomer|null,
    saveCustomerEvent:(customer:ICustomer) => void,
    editCustomerEvent: (customer:ICustomer|null) => void,
    cancelEvent : () => void
}

const CustomersDashboard = ({customers, editCustomer, isListLoaded, editCustomerEvent, cancelEvent, saveCustomerEvent, selectedCustomer}: IProps) => {
    return(

        <React.Fragment>

            {
                editCustomer === false && isListLoaded === true &&
                <CustomerList 
                customers={customers}
                editCustomerEvent={editCustomerEvent}
                />
            }
            {
                editCustomer && isListLoaded === true && 
                <CustomerEdit cancelEvent={cancelEvent}
                selectedCustomer={selectedCustomer}
                saveCustomerEvent={saveCustomerEvent}/>
            }
            {
                isListLoaded === false && <div>Loading</div>
            }
        </React.Fragment>
    )
}

export default CustomersDashboard;