using Api.Services;
using Api.Services.Interfaces;
using Api.Repositories;
using Api.Repositories.Interfaces;
using Api.Extensions;

var builder = WebApplication.CreateBuilder(args);

var corsPolicy = "corsPolicy";


builder.Services.AddCors(options =>
{

    options.AddPolicy(corsPolicy, configurePolicy =>
    {

        configurePolicy.AllowAnyMethod().AllowAnyHeader().WithOrigins("*");

    });

});
// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<ICustomerService, CustomerService>();
builder.Services.AddScoped<ICustomerRepository, CustomerRepository>();
//builder.Services.AddSingleton<ICustomerRepository, CustomerInMemoryRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.ConfigureExceptionHandler();
//app.ConfigurationExeptionHandler();

app.UseCors(corsPolicy);

app.UseAuthorization();

app.MapControllers();

app.Run();
