using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.DataAccess.Interfaces
{
    public interface IData
    {
      IDbConnection DbConnection{get;}  
    }
}