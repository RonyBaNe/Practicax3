using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using MySqlConnector;
using Api.DataAccess.Interfaces;

namespace Api.DataAccess
{
    public class Data:IData
    {
        private readonly IConfiguration _configuration;
        private string _connectionString = "Database connection";

        private MySqlConnection _conn;

        public Data(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IDbConnection DbConnection
        {
            get
            {
                if (_conn == null)
                {
                    _conn = new MySqlConnection(_configuration.GetConnectionString(_connectionString));
                }
                return _conn;
            }
        }
    }
}