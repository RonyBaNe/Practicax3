using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DataAccess.Interfaces;
using Api.Repositories.Interfaces;
using Api.Services.Interfaces;
using Dapper;
using Dapper.Contrib;
using Dapper.Contrib.Extensions;
using Entities;

namespace Api.Repositories
{
    public class CustomerRepository: ICustomerRepository
    {
        private readonly IData _data;
        CustomerRepository(IData data)
        {
            _data = data;
        }
        public async Task<List<Customer>> GetAllAsync()
        {
            //_data.DbConnection.
            var sql = "SELECT * FROM Customer";
            var customers = (await _data.DbConnection.QueryAsync<Customer>(sql)).ToList();

            return customers;

        }
        public async Task<bool> DeleteAsync(int id)
        {
            return await _data.DbConnection.DeleteAsync<Customer>(new Customer{Id = id});
        }


        public async Task<Customer> GetCustomerByIdAsync(int id)
        {
            var sql = "SELECT * FROM Customer WHERE Id =@CustomerId";
            
            var customer = (await _data.DbConnection.QueryAsync<Customer>(sql, new {GetCustomerByIdAsync = id})).ToList().FirstOrDefault();  

            return customer;
        }

        public async Task<Customer> SaveAsync(Customer customer)
        {
            var id = await _data.DbConnection.InsertAsync<Customer>(customer);

            customer.Id = id;

            return customer;
        }

        public async Task<Customer> UpdateAsync(Customer customer)
        {
            await _data.DbConnection.UpdateAsync(customer);

            return customer;
        }
    }
}